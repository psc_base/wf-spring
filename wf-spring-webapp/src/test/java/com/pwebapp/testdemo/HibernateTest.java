package com.pwebapp.testdemo;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.pwebapp.model.EmpModel;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/META-INF/spring/web-context/applicationContext-root.xml" })
@ActiveProfiles(profiles = "dev")
public class HibernateTest {

    private static final Logger logger = LogManager.getLogger(HibernateTest.class);

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    @Transactional
    public void TestDemo() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "select new com.pwebapp.model.EmpModel(p.ename,p.job,p.hiredate,p.sal,p.comm) from Emp p order by p.ename desc");
        @SuppressWarnings("unchecked")
        List<EmpModel> list = query.list();
        for (EmpModel model : list) {
            logger.info(model);
        }
    }

}
