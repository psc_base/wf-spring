package com.pwebapp.testdemo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.pwebapp.database.procedure.PSysDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/META-INF/spring/web-context/applicationContext-root.xml" })
@ActiveProfiles(profiles = "dev")
public class TestDemo {

    private static final Logger logger = LogManager.getLogger(TestDemo.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PSysDate pdbdate;

    @Test
    @Ignore
    public void TestDemo() {
        logger.info("Test is Test Class");
        Date date = jdbcTemplate.queryForObject("select sysdate from dual", Date.class);
        logger.info("Date => " + date);
        Date date1 = jdbcTemplate.queryForObject("select sysdate from dual", Date.class);
        logger.info("Date => " + date1);
    }

    @Test
    public void TestPreparedDemo() {
        Date date = jdbcTemplate.query(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement("select sysdate from dual");
                return ps;
            }
        }, new ResultSetExtractor<Date>() {

            @Override
            public Date extractData(ResultSet rs) throws SQLException, DataAccessException {
                if (rs.next()) {
                    return rs.getDate("sysdate");
                }
                return null;
            }
        });
        logger.info(date);
    }

    @Test
    @Ignore
    public void TestSysDateProc() {
        java.sql.Date date = pdbdate.execute();
        logger.info(date);
        java.sql.Date date1 = pdbdate.execute();
        logger.info(date1);
    }
}
